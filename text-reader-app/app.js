const fs = require('fs');
const encoding = 'utf8';

console.log('>>>', 'STARTING APPLICATON!!!');

const arguments = process.argv.slice(2);
if (arguments.length === 0) {
    return console.log('<<<', 'You need to pass the file name.');
}

const file = `./${arguments[0].replace(/\s/g, '').replace(/.txt/g, '')}.txt`;
console.log('>>>', 'file:', file);

readByRows(file);

async function readByRows(file) {
    return new Promise(resolve => {
        const readFile = `Read file [${file}]`;
        const process = 'Process time';
        const writeResult = 'Write result';

        console.time(readFile);
        const stream = fs.createReadStream(file, { encoding });
        stream.on('data', data => {
            const words = cleanWords(data);
            const wordsLen = words.length;
            const minWordsBlockEval = 2;
            let midLeng = !!(wordsLen % 2) ? (wordsLen + 1) / 2 : wordsLen / 2;
            let index = 0;
            const wordsBlock = [];
            wordsBlock.push({ quantity: 0, meesage: 'Total number of words evaluated' });

            console.log('>>>', `Reading process begins, rows = [${wordsLen}], middle length = [${midLeng}]`);

            console.time(process);
            for (let i = 0; i < wordsLen && midLeng >= minWordsBlockEval; i++, midLeng--) {

                for (let k = 0; k < wordsLen; k++) {

                    let textBlock = '';
                    const endBlock = midLeng + k;
                    for (let m = k; m < wordsLen && m < endBlock; m++) {
                        textBlock += `${words[m]} `;

                        if ((m + 1) % endBlock === 0) {
                            let existBlock = false;
                            textBlock = textBlock.trim();
                            wordsBlock.forEach(element => {
                                if (element.textBlock === textBlock) {
                                    element.quantity++;
                                    existBlock = true;
                                }
                            });
                            if (!existBlock) {
                                wordsBlock.push({
                                    index: ("00" + ++index).slice(-3),
                                    wordsQuantity: midLeng,
                                    quantity: 1,
                                    length: ("0" + textBlock.length).slice(-2),
                                    textBlock
                                });
                            }
                            textBlock = '';
                        }
                    }
                }
            }
            console.timeEnd(process);
            console.log('>>>', 'Ending process...');

            console.time(writeResult);
            wordsBlock.forEach(wordBlock => {
                if (wordBlock.quantity > 1) {
                    console.log(JSON.stringify(wordBlock));
                }
            })
            console.timeEnd(writeResult);
            console.log('>>>', 'writing result');

            stream.destroy();
        });
        stream.on('close', () => {
            console.timeEnd(readFile);
            console.log('>>>', 'ENDING APPLICATION!!!');
            resolve();
        });
    });
}

function cleanWords(data) {
    console.log('text: [', data, ']');
    const wordsCut = data.split(/\s/);
    const words = [];
    wordsCut.forEach(word => {
        const cleanWord = word.replace(/\s/g, '');
        if (cleanWord) words.push(cleanWord);
    });
    return words;
}
