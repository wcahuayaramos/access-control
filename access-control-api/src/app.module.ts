import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from './config/config.module';
import databaseConfig from './config/database.config';
import { ConfigService } from './config/config.service';
import { AuthModule } from './auth/auth.module';
import { BusinessModule } from './business/business.module';

@Module({
    imports: [
        TypeOrmModule.forRootAsync({
            imports: [ConfigModule],
            useFactory: databaseConfig,
            inject: [ConfigService],
        }),
        AuthModule,
        BusinessModule,
    ],
})
export class AppModule { }