import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { User } from '@entities/user.entity';
import * as bcrypt from 'bcryptjs';

@Injectable()
export class UserService extends TypeOrmCrudService<User> {
    constructor(
        @InjectRepository(User) repo) {
        super(repo);
    }

    async findByUsername(username: string): Promise<User> {
        return this.repo.findOne({ where: { username } });
    }

    async findByGmail(email: string): Promise<User> {
        return this.repo.findOne({ where: { email } });
    }

    async save(user: User): Promise<User> {
        user.password = await bcrypt.hash(user.password, 10);
        return this.repo.save(user);
    }

    async update(userId: string, userUpdate: User): Promise<number> {
        const userFields = {
            fullname: userUpdate.fullname,
            password: userUpdate.password,
        };
        let affectedRows = 0;
        try {
            const updateResult = await this.repo.update(userId, userFields);
            affectedRows = updateResult.raw.affectedRows;
            if (affectedRows < 1) { throw new BadRequestException('Error al actualizar el usuario.'); }
        } catch (error) { throw new BadRequestException('Error al actualizar', error); }
        return affectedRows;
    }

}
