import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Permission } from '../entities/permission.entity';

@Injectable()
export class PermissionService extends TypeOrmCrudService<Permission> {
    constructor(
        @InjectRepository(Permission) repo) {
        super(repo);
    }

    async findAll(): Promise<Permission[]> {
        const qb = this.repo.createQueryBuilder('permission')
            .leftJoinAndSelect('permission.user', 'user')
            .leftJoinAndSelect('permission.role', 'role')
            .addOrderBy('role.description', 'ASC');
        return await qb.getMany();
    }

}
