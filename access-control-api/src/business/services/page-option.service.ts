import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { PageOption } from '../entities/page-option.entity';

@Injectable()
export class PageOptionService extends TypeOrmCrudService<PageOption> {
    constructor(
        @InjectRepository(PageOption) repo) {
        super(repo);
    }

    findAll(): Promise<PageOption[]> {
        return this.repo.createQueryBuilder('page_option')
            .addOrderBy('description').getMany();
    }

}
