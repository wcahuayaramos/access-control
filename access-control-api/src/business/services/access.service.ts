import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Access } from '../entities/access.entity';

@Injectable()
export class AccessService extends TypeOrmCrudService<Access> {
    constructor(
        @InjectRepository(Access) repo) {
        super(repo);
    }

    async findAll(): Promise<Access[]> {
        const qb = this.repo.createQueryBuilder('access')
            .leftJoinAndSelect('access.role', 'role')
            .leftJoinAndSelect('access.pageOption', 'page_option')
            .addOrderBy('page_option.description', 'ASC');
        return await qb.getMany();
    }

}
