import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Store } from '@entities/store.entity';

@Injectable()
export class StoreService extends TypeOrmCrudService<Store> {
    constructor(
        @InjectRepository(Store) repo) {
        super(repo);
    }

    findById(id: string): Promise<Store> {
        return this.repo.findOne({ where: { id } });
    }

}
