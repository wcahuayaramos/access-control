import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Company } from '@entities/company.entity';

@Injectable()
export class CompanyService extends TypeOrmCrudService<Company> {
    constructor(
        @InjectRepository(Company) repo) {
        super(repo);
    }

    findByRuc(ruc: string): Promise<Company> {
        return this.repo.findOne({ where: { ruc } });
    }

}
