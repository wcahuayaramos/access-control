import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from 'src/business/controllers/app.controller';
import { UserController } from 'src/business/controllers/user.controller';
import { UserService } from 'src/business/services/user.service';
import { User } from 'src/business/entities/user.entity';
import { AppService } from '@services/app.service';
import { ConfigModule } from 'src/config/config.module';
import { ConfigService } from 'src/config/config.service';
import { ConfigKey } from 'src/config/configkey.enum';
import { Company } from '@entities/company.entity';
import { Store } from '@entities/store.entity';
import { CompanyService } from '@services/company.service';
import { StoreService } from '@services/store.service';
import { StoreController } from '@controllers/store.controller';
import { CompanyController } from '@controllers/company.controller';
import { Permission } from './entities/permission.entity';
import { Role } from './entities/role.entity';
import { RoleService } from './services/role.service';
import { PermissionService } from './services/permission.service';
import { RoleController } from './controllers/role.controller';
import { PermissionController } from './controllers/permission.controller';
import { Access } from './entities/access.entity';
import { PageOptionService } from './services/page-option.service';
import { PageOption } from './entities/page-option.entity';
import { AccessService } from './services/access.service';
import { AccessController } from './controllers/access.controller';
import { PageOptionController } from './controllers/page-option.controller';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            Access,
            Company,
            PageOption,
            Permission,
            Role,
            Store,
            User,
        ]),
    ],
    exports: [
        UserService,
    ],
    providers: [
        AccessService,
        AppService,
        CompanyService,
        PageOptionService,
        PermissionService,
        RoleService,
        StoreService,
        UserService,
    ],
    controllers: [
        AccessController,
        AppController,
        CompanyController,
        PageOptionController,
        PermissionController,
        RoleController,
        StoreController,
        UserController,
    ],
})
export class BusinessModule { }
