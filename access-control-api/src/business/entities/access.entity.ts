import { Entity, ManyToOne, JoinTable } from 'typeorm';
import { CrudValidationGroups } from '@nestjsx/crud';
import { BaseEntity } from '@entities/base/base.entity';
import { Role } from './role.entity';
import { IsOptional, IsDefined, IsNotEmptyObject } from 'class-validator';
import { PageOption } from './page-option.entity';

const { CREATE, UPDATE } = CrudValidationGroups;

@Entity()
export class Access extends BaseEntity {

    @IsOptional({ groups: [UPDATE] })
    @IsDefined({ groups: [CREATE] })
    @IsNotEmptyObject()
    @ManyToOne(() => Role, r => r.accesses)
    @JoinTable()
    role: Role;

    @IsOptional({ groups: [UPDATE] })
    @IsDefined({ groups: [CREATE] })
    @IsNotEmptyObject()
    @ManyToOne(() => PageOption, po => po.accesses)
    @JoinTable()
    pageOption: PageOption;

}
