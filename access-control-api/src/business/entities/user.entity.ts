import { Entity, Column, BeforeInsert, OneToMany } from 'typeorm';
import { IsOptional, IsString, IsNotEmpty } from 'class-validator';
import { CrudValidationGroups } from '@nestjsx/crud';
import { BaseEntity } from '@entities/base/base.entity';
import * as bcrypt from 'bcryptjs';
import { Permission } from './permission.entity';

const { CREATE, UPDATE } = CrudValidationGroups;

@Entity()
export class User extends BaseEntity {

    @IsOptional({ groups: [UPDATE] })
    @IsNotEmpty({ groups: [CREATE] })
    @IsString({ always: true })
    @Column({ type: 'varchar', length: 50, nullable: false, unique: true })
    username: string;

    @IsOptional({ groups: [UPDATE] })
    @IsNotEmpty({ groups: [CREATE] })
    @IsString({ always: true })
    @Column({ type: 'varchar', length: 50, nullable: false, unique: true })
    email: string;

    @IsOptional({ groups: [UPDATE] })
    @IsNotEmpty({ groups: [CREATE] })
    @IsString({ always: true })
    @Column({ type: 'varchar', length: 255, nullable: false })
    password: string;

    @IsOptional({ groups: [UPDATE] })
    @IsNotEmpty({ groups: [CREATE] })
    @IsString({ always: true })
    @Column({ type: 'varchar', length: 200, nullable: false })
    fullname: string;

    @IsOptional({ groups: [UPDATE] })
    @IsOptional({ groups: [CREATE] })
    @IsString({ always: true })
    @Column({ type: 'varchar', length: 100, default: '' })
    role: string;

    @IsOptional({ groups: [UPDATE] })
    @IsOptional({ groups: [CREATE] })
    @IsString({ always: true })
    @Column({ type: 'varchar', length: 100, default: '' })
    storeId: string;

    @OneToMany(() => Permission, ur => ur.role)
    permissions: Permission[];

    @BeforeInsert()
    async hashPassword() {
        this.password = await bcrypt.hash(this.password, 10);
    }

}
