import { Entity, Column, ManyToOne, JoinTable } from 'typeorm';
import { IsOptional, IsString, IsNotEmpty, IsDefined, IsNotEmptyObject } from 'class-validator';
import { CrudValidationGroups } from '@nestjsx/crud';
import { BaseEntity } from '@entities/base/base.entity';
import { Company } from '@entities/company.entity';

const { CREATE, UPDATE } = CrudValidationGroups;

@Entity()
export class Store extends BaseEntity {

    @IsOptional({ groups: [UPDATE] })
    @IsNotEmpty({ groups: [CREATE] })
    @IsString({ always: true })
    @Column({ type: 'varchar', length: 200, default: '' })
    commercialDescription: string;

    @IsOptional({ groups: [UPDATE] })
    @IsOptional({ groups: [CREATE] })
    @IsString({ always: true })
    @Column({ type: 'varchar', length: 10, default: '' })
    salesCode: string;

    @IsOptional({ groups: [UPDATE] })
    @IsOptional({ groups: [CREATE] })
    @IsString({ always: true })
    @Column({ type: 'varchar', length: 100, default: '' })
    email: string;

    @IsOptional({ groups: [UPDATE] })
    @IsNotEmpty({ groups: [CREATE] })
    @IsOptional({ always: true })
    @Column({ type: 'varchar', length: 500, default: '' })
    address: string;

    @IsOptional({ groups: [UPDATE] })
    @IsOptional({ groups: [CREATE] })
    @IsOptional({ always: true })
    @Column({ type: 'varchar', length: 10, default: '' })
    zipCode: string;

    @IsOptional({ groups: [UPDATE] })
    @IsDefined({ groups: [CREATE] })
    @IsNotEmptyObject()
    @ManyToOne(type => Company, c => c.stores)
    @JoinTable()
    company: Company;

}
