import { Entity, Column, OneToMany } from 'typeorm';
import { IsOptional, IsString, IsNotEmpty } from 'class-validator';
import { CrudValidationGroups } from '@nestjsx/crud';
import { BaseEntity } from '@entities/base/base.entity';
import { Permission } from './permission.entity';
import { Access } from './access.entity';

const { CREATE, UPDATE } = CrudValidationGroups;

@Entity()
export class Role extends BaseEntity {

    @IsOptional({ groups: [UPDATE] })
    @IsNotEmpty({ groups: [CREATE] })
    @IsString({ always: true })
    @Column({ type: 'varchar', length: 100, unique: true })
    description: string;

    @IsOptional({ groups: [UPDATE] })
    @IsOptional({ groups: [CREATE] })
    @IsString({ always: true })
    @Column({ type: 'varchar', length: 50, default: '' })
    abbreviation: string;

    @OneToMany(() => Permission, ur => ur.user)
    permissions: Permission[];

    @OneToMany(() => Access, rp => rp.role)
    accesses: Access[];

}
