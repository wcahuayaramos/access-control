import { Entity, ManyToOne, JoinTable } from 'typeorm';
import { CrudValidationGroups } from '@nestjsx/crud';
import { BaseEntity } from '@entities/base/base.entity';
import { User } from './user.entity';
import { Role } from './role.entity';
import { IsOptional, IsDefined, IsNotEmptyObject } from 'class-validator';

const { CREATE, UPDATE } = CrudValidationGroups;

@Entity()
export class Permission extends BaseEntity {

    @IsOptional({ groups: [UPDATE] })
    @IsDefined({ groups: [CREATE] })
    @IsNotEmptyObject()
    @ManyToOne(() => User, u => u.permissions)
    @JoinTable()
    user: User;

    @IsOptional({ groups: [UPDATE] })
    @IsDefined({ groups: [CREATE] })
    @IsNotEmptyObject()
    @ManyToOne(() => Role, r => r.permissions)
    @JoinTable()
    role: Role;

}
