import { Entity, Column, OneToMany } from 'typeorm';
import { IsOptional, IsString, IsNotEmpty } from 'class-validator';
import { CrudValidationGroups } from '@nestjsx/crud';
import { BaseEntity } from '@entities/base/base.entity';
import { Access } from './access.entity';

const { CREATE, UPDATE } = CrudValidationGroups;

@Entity()
export class PageOption extends BaseEntity {

    @IsOptional({ groups: [UPDATE] })
    @IsNotEmpty({ groups: [CREATE] })
    @IsString({ always: true })
    @Column({ type: 'varchar', length: 500, nullable: false, unique: true })
    description: string;

    @IsOptional({ groups: [UPDATE] })
    @IsOptional({ groups: [CREATE] })
    @IsString({ always: true })
    @Column({ type: 'varchar', length: 100, default: '' })
    idTop: string;

    @IsOptional({ groups: [UPDATE] })
    @IsOptional({ groups: [CREATE] })
    @IsString({ always: true })
    @Column({ type: 'varchar', length: 500, default: '' })
    descriptionTop: string;

    @IsOptional({ groups: [UPDATE] })
    @IsOptional({ groups: [CREATE] })
    @IsString({ always: true })
    @Column({ type: 'varchar', length: 50, default: '' })
    abbreviation: string;

    @OneToMany(() => Access, rp => rp.pageOption)
    accesses: Access[];

}
