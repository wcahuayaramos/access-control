import { PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, Column } from 'typeorm';
import { IsOptional, IsBoolean } from 'class-validator';
import { CrudValidationGroups } from '@nestjsx/crud';

const { CREATE, UPDATE } = CrudValidationGroups;

export class BaseEntity {

    @IsOptional({ always: true })
    @PrimaryGeneratedColumn("uuid")
    id?: string;

    @CreateDateColumn({ nullable: true })
    createdAt?: Date;

    @UpdateDateColumn({ nullable: true })
    updatedAt?: Date;

    @IsOptional({ groups: [UPDATE] })
    @IsOptional({ groups: [CREATE] })
    @IsBoolean({ always: true })
    @Column({ type: 'boolean', default: true })
    status: boolean;
}
