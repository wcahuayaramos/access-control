import { Entity, Column, OneToMany } from 'typeorm';
import { IsOptional, IsString, IsNotEmpty } from 'class-validator';
import { CrudValidationGroups } from '@nestjsx/crud';
import { BaseEntity } from '@entities/base/base.entity';
import { Store } from '@entities/store.entity';

const { CREATE, UPDATE } = CrudValidationGroups;

@Entity()
export class Company extends BaseEntity {

    @IsOptional({ groups: [UPDATE] })
    @IsNotEmpty({ groups: [CREATE] })
    @IsString({ always: true })
    @Column({ type: 'varchar', length: 11, default: '', unique: true })
    ruc: string;

    @IsOptional({ groups: [UPDATE] })
    @IsOptional({ groups: [CREATE] })
    @IsString({ always: true })
    @Column({ type: 'varchar', length: 200, default: '' })
    legalDescription: string;

    @IsOptional({ groups: [UPDATE] })
    @IsOptional({ groups: [CREATE] })
    @IsString({ always: true })
    @Column({ type: 'varchar', length: 200, default: '' })
    commercialDescription: string;

    @IsOptional({ groups: [UPDATE] })
    @IsOptional({ groups: [CREATE] })
    @IsString({ always: true })
    @Column({ type: 'varchar', length: 100, default: '' })
    email: string;

    @IsOptional({ groups: [UPDATE] })
    @IsOptional({ groups: [CREATE] })
    @IsString({ always: true })
    @Column({ type: 'varchar', length: 500, default: '' })
    address: string;

    @IsOptional({ groups: [UPDATE] })
    @IsOptional({ groups: [CREATE] })
    @IsString({ always: true })
    @Column({ type: 'varchar', length: 10, default: '' })
    zipCode: string;

    @OneToMany(type => Store, c => c.company)
    stores: Store[];
}
