import { Controller, UseGuards, Get } from '@nestjs/common';
import { Crud } from '@nestjsx/crud';
import { AuthGuard } from '@nestjs/passport';
import { Access } from '../entities/access.entity';
import { AccessService } from '../services/access.service';

@Crud({
    model: { type: Access },
    params: { id: { type: 'uuid', primary: true, field: 'id' } },
})
@UseGuards(AuthGuard('jwt'))
@Controller('accesses')
export class AccessController {
    constructor(public service: AccessService) { }

    @Get('all')
    findAll(): Promise<Access[]> { return this.service.findAll(); }
}
