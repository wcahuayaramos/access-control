import { Controller, UseGuards, Get } from '@nestjs/common';
import { Crud } from '@nestjsx/crud';
import { AuthGuard } from '@nestjs/passport';
import { CompanyService } from '@services/company.service';
import { Company } from '@entities/company.entity';

@Crud({
    model: { type: Company },
    params: { id: { type: 'uuid', primary: true, field: 'id' } },
})
@UseGuards(AuthGuard('jwt'))
@Controller('companies')
export class CompanyController {
    constructor(public service: CompanyService) { }

    @Get('assigned')
    findAssigned(): Promise<Company> {
        const ruc = "12345123456";
        return this.service.findByRuc(ruc);
    }
}
