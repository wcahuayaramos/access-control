import { Controller, UseGuards, Get } from '@nestjs/common';
import { Crud } from '@nestjsx/crud';
import { AuthGuard } from '@nestjs/passport';
import { StoreService } from '@services/store.service';
import { Store } from '@entities/store.entity';

@Crud({
    model: { type: Store },
    params: { id: { type: 'uuid', primary: true, field: 'id' } },
})
@UseGuards(AuthGuard('jwt'))
@Controller('stores')
export class StoreController {
    constructor(public service: StoreService) { }

    @Get('assigned')
    findAssigned(): Promise<Store> {
        const id = "e03439cb-36c5-4107-b881-377e3496545d";
        return this.service.findById(id);
    }
}
