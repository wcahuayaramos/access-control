import { Controller, UseGuards, Get } from '@nestjs/common';
import { Crud } from '@nestjsx/crud';
import { AuthGuard } from '@nestjs/passport';
import { RoleService } from '../services/role.service';
import { Role } from '../entities/role.entity';

@Crud({
    model: { type: Role },
    params: { id: { type: 'uuid', primary: true, field: 'id' } },
})
@UseGuards(AuthGuard('jwt'))
@Controller('roles')
export class RoleController {
    constructor(public service: RoleService) { }

    @Get('all')
    findAll(): Promise<Role[]> { return this.service.findAll(); }

}
