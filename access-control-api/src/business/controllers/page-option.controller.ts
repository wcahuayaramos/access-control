import { Controller, UseGuards, Get } from '@nestjs/common';
import { Crud } from '@nestjsx/crud';
import { AuthGuard } from '@nestjs/passport';
import { PageOption } from '../entities/page-option.entity';
import { PageOptionService } from '../services/page-option.service';

@Crud({
    model: { type: PageOption },
    params: { id: { type: 'uuid', primary: true, field: 'id' } },
})
@UseGuards(AuthGuard('jwt'))
@Controller('pageoptions')
export class PageOptionController {
    constructor(public service: PageOptionService) { }

    @Get('all')
    findAll(): Promise<PageOption[]> { return this.service.findAll(); }

}
