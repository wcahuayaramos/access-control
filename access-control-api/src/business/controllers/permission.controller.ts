import { Controller, UseGuards, Get } from '@nestjs/common';
import { Crud } from '@nestjsx/crud';
import { AuthGuard } from '@nestjs/passport';
import { PermissionService } from '../services/permission.service';
import { Permission } from '../entities/permission.entity';

@Crud({
    model: { type: Permission },
    params: { id: { type: 'uuid', primary: true, field: 'id' } },
})
@UseGuards(AuthGuard('jwt'))
@Controller('permissions')
export class PermissionController {
    constructor(public service: PermissionService) { }

    @Get('all')
    findAll(): Promise<Permission[]> { return this.service.findAll(); }
}
