import { Get, Controller, UnprocessableEntityException } from '@nestjs/common';
import { AppService } from '@services/app.service';
@Controller('app')
export class AppController {

    constructor(
        public appService: AppService,
    ) { }

    @Get('test_hello')
    testHello(): string {
        return this.appService.getHello();
    }

    @Get('test_exception')
    testException(): string {
        throw new UnprocessableEntityException('Test de error de exception.');
    }

}
