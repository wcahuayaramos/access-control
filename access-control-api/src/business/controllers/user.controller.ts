import { Controller, Get, Param, UseGuards, Post, Body, NotAcceptableException } from '@nestjs/common';
import { Crud } from '@nestjsx/crud';
import { UserService } from '@services/user.service';
import { User } from 'src/business/entities/user.entity';
import { AuthGuard } from '@nestjs/passport';

@Crud({
    model: { type: User },
    params: { id: { type: 'uuid', primary: true, field: 'id' } },
})
@Controller('users')
export class UserController {
    constructor(public service: UserService) { }

    @UseGuards(AuthGuard('jwt'))
    @Get('username/:username')
    async findByUserna(@Param('username') username): Promise<User> {
        const user = await this.service.findByUsername(username);
        user.password = '';
        return user;
    }

    @Post('/default')
    async save(@Body() user: User): Promise<User> {
        if (user && user.username !== 'developer') {
            throw new NotAcceptableException('Default user not allowed');
        }
        return await this.service.save(user);
    }

}
