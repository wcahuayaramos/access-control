import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import { HttpExceptionFilter } from './commons/filters/http-exception.filter';
import { ConfigKey } from './config/configkey.enum';
import { ExceptionInterceptor } from './commons/interceptors/exception.interceptor';

async function bootstrap() {
    const app = await NestFactory.create(AppModule);
    app.useGlobalFilters(new HttpExceptionFilter());
    app.useGlobalInterceptors(new ExceptionInterceptor());
    app.useGlobalPipes(new ValidationPipe());
    app.enableCors();
    await app.listen(app.get('ConfigService').get(ConfigKey.PORT_API));
}
bootstrap();