import { ArgumentsHost, Catch, ExceptionFilter, HttpException } from '@nestjs/common';
import { ErrorModel } from '../models/error.model';
import { ExceptionModel } from '../models/exception.model';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter<HttpException> {

    catch(httpException: HttpException, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const request = ctx.getRequest();
        const response = ctx.getResponse();
        const statusCode = httpException.getStatus();
        const exception = httpException.getResponse() as ExceptionModel;

        const errorModel = new ErrorModel();
        errorModel.statusCode = statusCode;
        errorModel.statusDescription = exception && exception.error ? exception.error : 'Server error without description';
        errorModel.statusMessage = exception && exception.message ? exception.message : 'Server error without message';
        errorModel.timestamp = new Date().toISOString();
        errorModel.path = request.url;
        errorModel.exception = exception;

        response.status(statusCode).json(errorModel);
    }

}
