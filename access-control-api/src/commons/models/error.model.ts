export class ErrorModel {
    statusCode?: number;
    statusDescription?: string;
    statusMessage?: string;
    timestamp?: string;
    path?: string;
    exception?: any;
}
