import { Controller, Post, Request, UseGuards, Req, Res, Get } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthLocalService } from './authlocal.service';
import { ConfigKey } from 'src/config/configkey.enum';
import { ConfigService } from 'src/config/config.service';

@Controller('auth')
export class AuthLocalController {

    constructor(
        private readonly configService: ConfigService,
        private readonly authLocalService: AuthLocalService) { }

    /** Login local */

    @Post('login')
    @UseGuards(AuthGuard('local'))
    async login(@Request() req) {
        return this.authLocalService.login(req.user);
    }

    /** Login with google */

    @Get('google')
    @UseGuards(AuthGuard('google'))
    googleLogin() {
        /** Impl */
    }

    @Get('google/callback')
    @UseGuards(AuthGuard('google'))
    googleLoginCallback(@Req() req, @Res() res) {
        const jwt: string = req.user.jwt;
        const userid: string = req.user.userid;
        const username: string = req.user.username;
        const url = this.configService.getServer(ConfigKey.DOMAIN_SERVER, ConfigKey.PORT_WEB);
        if (jwt) {
            res.redirect(`${url}/login/${jwt}/${userid}/${username}`);
        } else {
            res.redirect(`${url}/login`);
        }
    }

    /** Test if the token is working */

    @UseGuards(AuthGuard('jwt'))
    @Get('protected')
    protectedResource() {
        return 'JWT is working protected!';
    }

    @Get('freed')
    freeResource() {
        return 'JWT is working freed!';
    }

}
