import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from 'src/config/config.service';
import { ConfigKey } from 'src/config/configkey.enum';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {

    constructor(private readonly configService: ConfigService) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: configService.get(ConfigKey.JWT_SECRET),
        });
    }

    async validate(payload: any, done: (error: any, user?: any) => void) {
        try {
            done(null, payload);
            return { id: payload.sub, username: payload.username };
        } catch (err) {
            throw new UnauthorizedException('Token no autorizado.', err.message);
        }
    }
}
