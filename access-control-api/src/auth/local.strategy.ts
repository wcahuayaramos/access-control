
import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthLocalService } from './authlocal.service';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy, 'local') {

    constructor(private readonly authLocalService: AuthLocalService) {
        super();
    }

    async validate(username: string, password: string): Promise<any> {
        let user = {};
        try {
            user = await this.authLocalService.validatePassOfUser(username, password);
            if (!user) {
                throw new UnauthorizedException('Usuario no autorizado.');
            }
        } catch (err) {
            throw new UnauthorizedException('Error al validar la autorización.', err.message);
        }
        return user;
    }
}
