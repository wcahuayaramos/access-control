import { Injectable } from '@nestjs/common';
import { UserService } from '../business/services/user.service';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcryptjs';

@Injectable()
export class AuthLocalService {
    constructor(
        private readonly userService: UserService,
        private readonly jwtService: JwtService,
    ) { }

    async validatePassOfUser(username: string, pass: string): Promise<any> {

        const user = await this.userService.findByUsername(username);
        if (user && (await bcrypt.compare(pass, user.password))) {
            const { password, ...userWithoutPassword } = user;
            return userWithoutPassword;
        }

        return null;
    }

    async login(user: any) {
        const payload = { username: user.username, sub: user.id };
        return {
            token: this.jwtService.sign(payload),
            user,
        };
    }

}
