import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { AuthLocalService } from './authlocal.service';
import { ConfigModule } from '../config/config.module';
import { ConfigService } from '../config/config.service';
import { LocalStrategy } from './local.strategy';
import { JwtStrategy } from './jwt.strategy';
import { BusinessModule } from '../business/business.module';
import { AuthLocalController } from './auth.controller';
import { ConfigKey } from 'src/config/configkey.enum';

@Module({
    imports: [
        BusinessModule,
        PassportModule,
        JwtModule.registerAsync({
            imports: [ConfigModule],
            useFactory: async (configService: ConfigService) => ({
                secret: configService.get(ConfigKey.JWT_SECRET),
            }),
            inject: [ConfigService],
        }),
    ],
    controllers: [AuthLocalController],
    providers: [
        AuthLocalService, LocalStrategy,
        JwtStrategy],
    exports: [AuthLocalService],
})
export class AuthModule { }
