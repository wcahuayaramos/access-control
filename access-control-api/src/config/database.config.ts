import { ConfigService } from 'src/config/config.service';
import { ConfigKey } from 'src/config/configkey.enum';

export default async (config: ConfigService) => ({
    type: 'mysql' as 'mysql',
    host: config.get(ConfigKey.DATABASE_HOST),
    port: Number(config.get(ConfigKey.DATABASE_PORT)),
    username: config.get(ConfigKey.DATABASE_USERNAME),
    password: config.get(ConfigKey.DATABASE_PASSWORD),
    database: config.get(ConfigKey.DATABASE_NAME),
    entities: [__dirname + '/../**/*.entity{.ts,.js}'],
    synchronize: true,
});
