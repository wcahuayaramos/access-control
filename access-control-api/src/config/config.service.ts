import * as dotenv from 'dotenv';
import * as fs from 'fs';

export class ConfigService {
    private readonly envConfig: Record<string, string>;

    constructor(filePath: string) {
        this.envConfig = dotenv.parse(fs.readFileSync(filePath));
    }

    get(key: string): string {
        return this.envConfig[key];
    }

    getServer(keyOne: string, keyTwo: string): string {
        return `${this.envConfig[keyOne]}${this.envConfig[keyTwo]}`;
    }
}
